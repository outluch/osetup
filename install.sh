#!/bin/bash

sudo apt-get install -y tmux zsh mosh ncdu htop iotop
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

CURDIR=$(pwd)

# zsh config
if ! [ -h ~/.zsh-osetup ]; then
    ln -s $CURDIR/zsh-osetup ~/.zsh-osetup
    echo ".zsh-osetup linked"
else
    echo "SKIP: .zsh-osetup already linked"
fi

if ! grep -Fxq "source ~/.zsh-osetup" ~/.zshrc; then
    echo "source ~/.zsh-osetup" >> ~/.zshrc
    echo "~/.zsh-osetup added to .zshrc"
else
    echo "SKIP: ~/.zsh-osetup already added to .zshrc"
fi

if ! [ -h ~/.tmux.conf ]; then
    if [ -f ~/.tmux.conf ]; then
        mv ~/.tmux.conf ~/.tmux.conf.bak
    fi
    ln -s $CURDIR/tmux.conf ~/.tmux.conf
    echo ".tmux.conf linked, old one is backed up with .bak"
else
    echo "SKIP: .tmux.conf already linked"
fi
